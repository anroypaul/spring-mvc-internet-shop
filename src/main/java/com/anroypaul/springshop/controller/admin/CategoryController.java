package com.anroypaul.springshop.controller.admin;

import com.anroypaul.springshop.entity.Category;
import com.anroypaul.springshop.service.category.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("admin/categories")
public class CategoryController {

    public final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public String getAllCategories(
            @RequestParam(value = "page", defaultValue = "1", required = false) int page,
            @RequestParam(value = "limit", defaultValue = "10", required = false) int limit,
            Model model) {
        int startItem = (page - 1) * limit;

        int allItems = categoryService.count();

        model.addAttribute("allItems", allItems);
        model.addAttribute("lastPage", allItems / 10 + 1);
        model.addAttribute("categories", categoryService.paginate(limit, startItem));
        return "admin/categories/list";
    }

    @GetMapping("/{id}")
    public String getCategoryById(@PathVariable("id") int id, Model model) {
        model.addAttribute("category", categoryService.getById(id));
        return "admin/categories/show";
    }

    @GetMapping("/add")
    public String addNew(Model model) {
        model.addAttribute("category", new Category());
        return "admin/categories/show";
    }

    @PostMapping()
    public String create(@ModelAttribute("category") Category category) {
        categoryService.save(category);
        return "redirect:/admin/categories";
    }

    @PostMapping("/{id}")
    public String update(@ModelAttribute("user") Category category) {
        categoryService.update(category);
        return "redirect:/admin/categories";
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable("id") int id) {
        categoryService.delete(id);
        return "redirect:/admin/categories";
    }
}
