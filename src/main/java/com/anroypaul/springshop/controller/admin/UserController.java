package com.anroypaul.springshop.controller.admin;

import com.anroypaul.springshop.entity.User;
import com.anroypaul.springshop.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("admin/users")
public class UserController {

    public final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getAllUsers(
            @RequestParam(value = "page", defaultValue = "1", required = false) int page,
            @RequestParam(value = "limit", defaultValue = "10", required = false) int limit,
            Model model) {
        int startItem = (page - 1) * limit;

        int allItems = userService.count();

        model.addAttribute("allItems", allItems);
        model.addAttribute("lastPage", allItems / 10 + 1);
        model.addAttribute("users", userService.paginate(limit, startItem));
        return "admin/users/list";
    }

    @GetMapping("/{id}")
    public String getUserById(@PathVariable("id") int id, Model model) {
        model.addAttribute("user", userService.getById(id));
        return "admin/users/show";
    }

    @GetMapping("/add")
    public String addNew(Model model) {
        model.addAttribute("user", new User());
        return "admin/users/show";
    }

    @PostMapping()
    public String create(@ModelAttribute("user") User user) {
        userService.save(user);
        return "redirect:/admin/users";
    }

    @PostMapping("/{id}")
    public String update(@ModelAttribute("user") User user) {
        userService.update(user);
        return "redirect:/admin/users";
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable("id") int id) {
        userService.delete(id);
        return "redirect:/admin/users";
    }
}
