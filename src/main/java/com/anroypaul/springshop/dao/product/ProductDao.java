package com.anroypaul.springshop.dao.product;

import com.anroypaul.springshop.dao.base.BaseRelationDao;
import com.anroypaul.springshop.entity.Category;
import com.anroypaul.springshop.entity.Product;

public interface ProductDao extends BaseRelationDao<Product, Category> {

}
