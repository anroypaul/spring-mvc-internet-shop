package com.anroypaul.springshop.dao.product;

import com.anroypaul.springshop.entity.Category;
import com.anroypaul.springshop.entity.Product;
import com.anroypaul.springshop.mapper.ProductWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class ProductDaoPostgresImpl implements ProductDao {

    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductDaoPostgresImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Product> findAll() {
        String sql = "SELECT products.*, categories.id AS category_id, categories.name AS category_name FROM products" +
                "INNER JOIN categories ON products.category_id = categories.id;";
        return jdbcTemplate.query(sql, new ProductWrapper());
    }

    @Override
    public List<Product> paginate(int limit, int offset) {
        String sql = "SELECT products.*, categories.id AS category_id, categories.name AS category_name FROM products" +
                "INNER JOIN categories ON products.category_id = categories.id LIMIT ? OFFSET ?";
        return jdbcTemplate.query(sql, new ProductWrapper(), limit, offset);
    }

    @Override
    public int count() {
        String sql = "SELECT COUNT(*) FROM products";
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public void save(Product model, Category category) {
        String sql = "INSERT INTO products (name, description, picture) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, model.getName(), model.getDescription(), model.getPicture());

    }

    @Override
    public void update(Product model, Category category) {
        String sql = "UPDATE products SET name = ?, description = ?, picture = ? WHERE id = ?";
        jdbcTemplate.update(sql, model.getName(), model.getDescription(), model.getPicture(), model.getId());

    }

    @Override
    public Product getById(int id) {
        String sql = "SELECT * FROM products WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new ProductWrapper(), id);

    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM products WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }
}
