package com.anroypaul.springshop.dao.base;

import java.util.List;

public interface BaseDao<T> {

    List<T> findAll();

    List<T> paginate(int limit, int offset);

    int count();

    void save(T model);

    void update(T model);

    T getById(int id);

    void delete(int id);
}
