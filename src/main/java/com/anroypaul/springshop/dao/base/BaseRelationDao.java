package com.anroypaul.springshop.dao.base;

import java.util.List;

public interface BaseRelationDao<T, R> {

    List<T> findAll();

    List<T> paginate(int limit, int offset);

    int count();

    void save(T child, R parent);

    void update(T child, R parent);

    T getById(int id);

    void delete(int id);

}
