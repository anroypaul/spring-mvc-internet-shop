package com.anroypaul.springshop.dao.user;

import com.anroypaul.springshop.entity.User;

import java.util.List;

public interface UserDao {

    List<User> findAll();

    List<User> paginate(int limit, int offset);

    int count();

    void save(User user);

    void update(User user);

    User getById(int id);

    void delete(int id);
}
