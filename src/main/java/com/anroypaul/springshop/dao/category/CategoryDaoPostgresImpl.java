package com.anroypaul.springshop.dao.category;

import com.anroypaul.springshop.entity.Category;
import com.anroypaul.springshop.mapper.CategoryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class CategoryDaoPostgresImpl implements CategoryDao {

    public final JdbcTemplate jdbcTemplate;

    @Autowired
    public CategoryDaoPostgresImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Category> findAll() {
        String sql = "SELECT * FROM categories";
        return jdbcTemplate.query(sql, new CategoryWrapper());
    }

    @Override
    public List<Category> paginate(int limit, int offset) {
        String sql = "SELECT * FROM categories LIMIT ? OFFSET ?";
        return jdbcTemplate.query(sql, new CategoryWrapper(), limit, offset);
    }

    @Override
    public int count() {
        String sql = "SELECT COUNT(*) FROM categories";
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public void save(Category model) {
        String sql = "INSERT INTO categories (name, description, picture) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, model.getName(), model.getDescription(), model.getPicture());

    }

    @Override
    public void update(Category model) {
        String sql = "UPDATE categories SET name = ?, description = ?, picture = ? WHERE id = ?";
        jdbcTemplate.update(sql, model.getName(), model.getDescription(), model.getPicture(), model.getId());

    }

    @Override
    public Category getById(int id) {
        String sql = "SELECT * FROM categories WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new CategoryWrapper(), id);

    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM categories WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }
}
