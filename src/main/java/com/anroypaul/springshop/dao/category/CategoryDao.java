package com.anroypaul.springshop.dao.category;

import com.anroypaul.springshop.dao.base.BaseDao;
import com.anroypaul.springshop.entity.Category;

public interface CategoryDao extends BaseDao<Category> {

}
