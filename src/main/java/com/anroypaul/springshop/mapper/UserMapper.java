package com.anroypaul.springshop.mapper;

import com.anroypaul.springshop.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setEmail(resultSet.getString("email"));
        user.setBirthDate(resultSet.getDate("birth_date"));
        user.setPhoneNumber(resultSet.getString("phone_number"));
        user.setIsEnabled(resultSet.getBoolean("enabled"));
        return user;
    }
}
