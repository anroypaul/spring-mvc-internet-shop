package com.anroypaul.springshop.config;

import com.anroypaul.springshop.dao.category.CategoryDao;
import com.anroypaul.springshop.dao.category.CategoryDaoPostgresImpl;
import com.anroypaul.springshop.dao.product.ProductDao;
import com.anroypaul.springshop.dao.product.ProductDaoPostgresImpl;
import com.anroypaul.springshop.dao.user.UserDao;
import com.anroypaul.springshop.dao.user.UserDaoPostgresImpl;
import com.anroypaul.springshop.service.category.CategoryService;
import com.anroypaul.springshop.service.category.CategoryServiceImpl;
import com.anroypaul.springshop.service.product.ProductService;
import com.anroypaul.springshop.service.product.ProductServiceImpl;
import com.anroypaul.springshop.service.user.UserService;
import com.anroypaul.springshop.service.user.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class SpringConfig {

//    @Bean
//    public TestBean getTestBean() {
//        return new TestBean("Hello");
//    }

    @Bean
    public JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(getDataSource());
    }

    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/springshop");
        dataSource.setUsername("postgres");
        dataSource.setPassword("");
        dataSource.setDriverClassName("org.postgresql.Driver");
        return dataSource;
    }

    @Bean
    public UserDao getUserDao() {
        return new UserDaoPostgresImpl(getJdbcTemplate());
    }

    @Bean
    public UserService getUserService() {
        return new UserServiceImpl();
    }

    @Bean
    public CategoryDao getCategoryDao() {
        return new CategoryDaoPostgresImpl(getJdbcTemplate());
    }

    @Bean
    public CategoryService getCategoryService() {
        return new CategoryServiceImpl();
    }

    @Bean
    public ProductDao getProductDao() { return new ProductDaoPostgresImpl(getJdbcTemplate()); }

    @Bean
    public ProductService getProductService() { return new ProductServiceImpl(); }

}
