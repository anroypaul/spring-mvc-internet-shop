package com.anroypaul.springshop.entity;

import java.math.BigDecimal;

public class OrderDetails {

    private int id;
    private Product product;
    private int quantity;
    private BigDecimal totalPrice;
    private Order order;

    public OrderDetails() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalPrice() {
        return this.product.getPrice().multiply(new BigDecimal(this.quantity));
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
