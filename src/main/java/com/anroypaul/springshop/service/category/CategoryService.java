package com.anroypaul.springshop.service.category;

import com.anroypaul.springshop.entity.Category;

import java.util.List;

public interface CategoryService {

    List<Category> findAll();

    List<Category> paginate(int limit, int offset);

    int count();

    void save(Category category);

    void update(Category category);

    Category getById(int id);

    void delete(int id);
}
