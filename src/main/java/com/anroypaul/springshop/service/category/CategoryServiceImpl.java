package com.anroypaul.springshop.service.category;

import com.anroypaul.springshop.dao.category.CategoryDao;
import com.anroypaul.springshop.entity.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    public CategoryDao categoryDao;

    @Override
    public List<Category> findAll() {
        return categoryDao.findAll();
    }

    @Override
    public List<Category> paginate(int limit, int offset) {
        return categoryDao.paginate(limit, offset);
    }

    @Override
    public int count() {
        return categoryDao.count();
    }

    @Override
    public void save(Category category) {
        categoryDao.save(category);
    }

    @Override
    public void update(Category category) {
        categoryDao.update(category);
    }

    @Override
    public Category getById(int id) {
        return categoryDao.getById(id);
    }

    @Override
    public void delete(int id) {
        categoryDao.delete(id);
    }
}
