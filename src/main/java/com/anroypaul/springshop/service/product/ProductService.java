package com.anroypaul.springshop.service.product;

import com.anroypaul.springshop.entity.Category;
import com.anroypaul.springshop.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> findAll();

    List<Product> paginate(int offset, int limit);

    int count();

    void save(Product user, Category category);

    void update(Product user, Category category);

    Product getById(int id);

    void delete(int id);
}
