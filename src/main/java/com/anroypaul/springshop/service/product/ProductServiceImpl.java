package com.anroypaul.springshop.service.product;

import com.anroypaul.springshop.dao.product.ProductDao;
import com.anroypaul.springshop.entity.Category;
import com.anroypaul.springshop.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProductServiceImpl implements ProductService {

    @Autowired
    public ProductDao productDao;

    public List<Product> findAll() {
        return productDao.findAll();
    }

    @Override
    public List<Product> paginate(int offset, int limit) {
        return productDao.paginate(offset, limit);
    }

    @Override
    public int count() {
        return productDao.count();
    }

    @Override
    public void save(Product product, Category category) {
        productDao.save(product, category);
    }

    @Override
    public void update(Product product, Category category) {
        productDao.update(product, category);
    }

    @Override
    public Product getById(int id) {
        return productDao.getById(id);
    }

    @Override
    public void delete(int id) {
        productDao.delete(id);
    }
}
