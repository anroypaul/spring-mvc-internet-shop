package com.anroypaul.springshop.service.user;

import com.anroypaul.springshop.entity.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    List<User> paginate(int offset, int limit);

    int count();

    void save(User user);

    void update(User user);

    User getById(int id);

    void delete(int id);

}
