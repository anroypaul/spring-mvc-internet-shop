package com.anroypaul.springshop.service.user;

import com.anroypaul.springshop.dao.user.UserDao;
import com.anroypaul.springshop.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    public UserDao userDao;

    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public List<User> paginate(int offset, int limit) {
        return userDao.paginate(offset, limit);
    }

    @Override
    public int count() {
        return userDao.count();
    }

    @Override
    public void save(User user) {
        userDao.save(user);
    }

    @Override
    public void update(User user) {
        userDao.update(user);
    }

    @Override
    public User getById(int id) {
        return userDao.getById(id);
    }

    @Override
    public void delete(int id) {
        userDao.delete(id);
    }
}
