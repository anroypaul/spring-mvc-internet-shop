package com.anroypaul.springshop.service;

public class TestBean {

    public String name;

    public TestBean() {

    }

    public TestBean(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
